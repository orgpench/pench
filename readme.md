<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: (C) 2021-2021, Jayesh Badwaik <jayesh@badwaik.dev>
-->
# PenchTeX

A toolkit to manage TeX classes, packages and templates.

**Sample Document**

- [Article](https://gitlab.com/orgpench/pench/-/jobs/artifacts/main/raw/artifact/article.pdf?job=article)
- [Beamer](https://gitlab.com/orgpench/pench/-/jobs/artifacts/main/raw/artifact/beamer.pdf?job=beamer)
- [Book](https://gitlab.com/orgpench/pench/-/jobs/artifacts/main/raw/artifact/book.pdf?job=book)
- [Report](https://gitlab.com/orgpench/pench/-/jobs/artifacts/main/raw/artifact/report.pdf?job=report)


## Usage

You can use an interactive version of the document generation using the script shown below:

```
./src/bin/penchtex.create.interactive
```

In case you want to use a non-interactive script, you can use
```
./src/bin/penchtex.create [article|beamer|book|report] <title enclosed in quotes> <directory to store document in>
```

## Installation

You can install this software in your computer using the following script.

```
./bin/install <install-prefix>
```

